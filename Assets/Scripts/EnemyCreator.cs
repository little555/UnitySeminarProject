﻿using UnityEngine;

public class EnemyCreator : MonoBehaviour
{
    /** 敵 */
    public GameObject enemy;

    /** 敵を作る閾値 */
    private float border = 80f;

    /** 敵が生成される距離 */
    private readonly float createDistance = 250f;

    /**
     * 更新処理
     * */
    void Update()
    {
        if (this.transform.position.z > this.border)
        {
            this.CreateEnemy();
        }
    }

    /**
     * 敵を作成します
     */
    private void CreateEnemy()
    {
        var count = Random.Range(0, 14);
        if (count > 8)
        {
            count -= 8;
            Instantiate(this.enemy, new Vector3(-5f, 0.8f, this.border + this.createDistance), this.enemy.transform.rotation);
        }
        if (count > 4)
        {
            count -= 4;
            Instantiate(this.enemy, new Vector3(-1.8f, 0.8f, this.border + this.createDistance), this.enemy.transform.rotation);
        }
        if (count > 2)
        {
            count -= 2;
            Instantiate(this.enemy, new Vector3(1.8f, 0.8f, this.border + this.createDistance), this.enemy.transform.rotation);
        }
        if (count > 1)
        {
            Instantiate(this.enemy, new Vector3(5f, 0.8f, this.border + this.createDistance), this.enemy.transform.rotation);
        }
        this.border += 80f;
    }
}
