﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDestroyer : MonoBehaviour {


    void OnTriggerEnter(Collider c)
    {    
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);

        if (layerName == "Player")
        {
            Destroy(this.gameObject.transform.parent.gameObject);
        }
    }
}
