﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RunUIController : MonoBehaviour {

    public GameObject player;

    private Text scoreGUI;
    private Text timeGUI;
    private Text gameOverGUI;
    private float time = 30;
    private int scoreBorder = 1000;
    private PlayerContoller playerContoller;

    // Use this for initialization
    void Start ()
    {
        foreach (Transform child in this.transform)
        {
            switch(child.name)
            {
                case "Score":
                    this.scoreGUI = child.GetComponent<Text>();
                    break;
                case "Time":
                    this.timeGUI = child.GetComponent<Text>(); ;
                    break;
                case "GameOver":
                    this.gameOverGUI = child.GetComponent<Text>();
                    this.gameOverGUI.enabled = false;
                    break;
            }
        }

        this.playerContoller = this.player.GetComponent<PlayerContoller>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        this.time -= 1f * Time.deltaTime;
        this.timeGUI.text = ((int)this.time).ToString();
        var score = (int)this.player.transform.position.z;
        this.scoreGUI.text = score.ToString();

        if (this.time < 0 || this.playerContoller.IsGameOver)
        {
            this.gameOverGUI.enabled = true;
            Time.timeScale = 0;
            if (Input.GetMouseButtonDown(0))
            {
                SceneManager.LoadScene("title");
            }
        }
        if (score > this.scoreBorder)
        {
            this.scoreBorder += 1000;
            var addTime = (15 - (scoreBorder / 1000));
            this.time += addTime < 0 ? 1 : addTime + 1;
        }
    }
}
