﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerContoller : MonoBehaviour {

    /** 加速度 */
    public float accPower = 1f;
    /** 減速度 */
    public float decPower = 0.3f;
    /** 横移動 */
    public float movePower = 0.2f;

    /** 速度 */
    private float speed = 0f;

    /** 剛体コンポーネント */
    private Rigidbody rb;

    public bool IsGameOver {
        private set;
        get;
    }

    /**
     * 初期化関数
     * */
    void Start()
    {
        // Rigidbody（剛体特性）を取得する
        this.rb = this.GetComponent<Rigidbody>();
    }

    /**
     * 更新処理
     * */
    void FixedUpdate()
    {
        if (Input.GetKey("up"))
        {
            this.Accel(); //アクセル
        }
        if (Input.GetKey("down"))
        {
            this.Back(); //バック
        }
        if (Input.GetKey("right"))
        {
            this.MoveSide(this.movePower); //右移動
        }
        if (Input.GetKey("left"))
        {
            this.MoveSide((-1f) * this.movePower); //左移動
        }

        this.SaveSpeed();

        this.Decelerate();

        this.rb.velocity = new Vector3(this.rb.velocity.x, this.rb.velocity.y, this.speed);
    }

    /**
     * 接触イベント
     * */
    void OnTriggerEnter(Collider c)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);

        if (layerName == "Enemy")
        {
            this.IsGameOver = true;
        }
    }

    /**
     * アクセル
     * */
    private void Accel()
    {
        this.speed += this.accPower;
    }

    /**
     * バック
     * */
    private void Back()
    {
        this.speed -= this.accPower / 3;
    }

    /**
     * 左右移動
     * */
    private void MoveSide(float move)
    {
        var nextPos = this.transform.position.x + move;

        if (-5f <= nextPos && nextPos <= 5f)
        {
            Vector3 temp = new Vector3(nextPos, this.transform.position.y, this.transform.position.z);
            this.transform.position = temp;
        }
    }

    /**
     * 速度制限
     * */
    private void SaveSpeed()
    {
        var maxSpeed = 100 + ((int)this.transform.position.z / 100);
        if (this.speed > maxSpeed)
        {
            this.speed = maxSpeed;
        }
    }


    /**
     * 自動減速
     * */
    private void Decelerate()
    {
        if (this.speed > 0)
        {
            var nextSpeed = this.speed - this.decPower;
            this.speed = nextSpeed > 0 ? nextSpeed : 0f;
        }
        else if (speed < 0)
        {
            var nextSpeed = this.speed + this.decPower;
            this.speed = nextSpeed < 0 ? nextSpeed : 0f;
        }
    }
}
